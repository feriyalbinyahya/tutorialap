package id.ac.ui.cs.advprog.tutorial1.strategy;

public class SwimNoWay implements SwimBehavior {

    @Override
    public void swim(){
        System.out.println("Bebek ini tidak dapat berenang.");
    }
}
