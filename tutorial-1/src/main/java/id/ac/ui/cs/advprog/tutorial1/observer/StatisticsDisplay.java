package id.ac.ui.cs.advprog.tutorial1.observer;

import java.util.Observable;
import java.util.Observer;

public class StatisticsDisplay implements Observer, DisplayElement {

    private float maxTemp = 0.0f;
    private float minTemp = 200;
    private float tempSum = 0.0f;
    private int numReadings;

    public StatisticsDisplay(Observable observable) {
        observable.addObserver(this);
    }

    @Override
    public void display() {
        System.out.println("Avg/Max/Min temperature = " + (tempSum / numReadings)
                + "/" + maxTemp + "/" + minTemp);
    }

    @Override
    public void update(Observable o, Object arg) {
        if (o instanceof WeatherData) {
            WeatherData data = (WeatherData) o;
            float databaru = data.getTemperature();
            tempSum += databaru;
            numReadings++;

            //kalau temp yg baru lebih besar dari maxtemp maka data maxtemp keganti
            if(databaru > maxTemp){
                this.maxTemp = databaru;
            }

            //kalau temp yg baru lebih kecil dari mintemp maka data mintemp keganti
            if(databaru < minTemp){
                this.minTemp = databaru;
            }

            display();  //ditampilkan


        }
    }
}
