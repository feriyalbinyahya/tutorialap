package id.ac.ui.cs.advprog.tutorial1.strategy;

public class MallardDuck extends Duck {
    MallardDuck(){
        setFlyBehavior(new FlyWithWings());
        setQuackBehavior(new Quack());
        setSwimBehavior(new Swim());
    }

    @Override
    public void display(){
        System.out.println("Ini MallardDuck!");
    }
}
