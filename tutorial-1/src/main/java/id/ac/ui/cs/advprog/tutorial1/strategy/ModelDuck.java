package id.ac.ui.cs.advprog.tutorial1.strategy;

public class ModelDuck extends Duck{
    ModelDuck(){
        setFlyBehavior(new FlyNoWay());
        setQuackBehavior(new MuteQuack());
        setSwimBehavior(new SwimNoWay());
    }

    @Override
    public void display(){
        System.out.println("Ini ModelDuck!");
    }
}
